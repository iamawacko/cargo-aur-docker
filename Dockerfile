FROM rust:stretch

RUN rustup install stable
RUN rustup default stable
RUN rustc --version && cargo --version

RUN cargo install cargo-aur

WORKDIR /home/
